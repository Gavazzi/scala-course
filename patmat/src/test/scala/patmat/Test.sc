import patmat.Huffman._

val testList = List(('a', 1), ('b', 2))
testList.patch(0, Seq(('a', 2)), 1)

val chars = List('a', 'b', 'c', 'c', 'a', 'a')

'X' :: chars

chars  :+ 's'

