class Rational(x: Int, y: Int) {
  require(y > 0, "denominator must be nonzero")
  private def gcd(a : Int, b : Int) : Int = if(b == 0) a else gcd(b, a % b)
  private def g = gcd(x, y)
  def numerator = x
  def denominator = y
  def less(that : Rational) : Boolean = {
    this.numerator * that.denominator < that.numerator * this.denominator
  }

  def max(that : Rational) : Rational = {
    if(less(that)) that else this
  }

  def add(that: Rational) : Rational = {
    new Rational(numerator * that.denominator + that.numerator * denominator, denominator * that.denominator)
  }
  def sub(that: Rational) : Rational = {
    add(neg(that))
  }
  def neg(that : Rational) : Rational = {
    new Rational(-that.numerator, that.denominator)
  }
  def div(that : Rational) : Rational = {
    mul(new Rational(that.denominator, that.numerator))
  }
  def mul(that : Rational) : Rational = {
    new Rational(that.numerator * numerator, that.denominator * that.denominator)
  }
  override def toString() : String = {
    numerator / g + "/" + denominator / g
  }
}

new Rational(1, 2).add(new Rational(2, 3))
new Rational(1, 2).sub(new Rational(1, 2))
new Rational(2, 2).mul(new Rational(1, 2))
new Rational(2, 2).div(new Rational(1, 2))

val x = new Rational(1, 3)
val y = new Rational(5, 7)
val z = new Rational(3, 2)

x.sub(y).sub(z)
y.add(y)

x.less(y)