def product(f: Int => Int) (a : Int, b: Int) : Int = {
  if(a > b) 1
  else f(a) * product(f)(a + 1, b)
}

product(x => x * x)(3, 4)

def factorial(n : Int) : Int = product(x => x)(1, n)

factorial(5)


def general(f: Int => Int)
           (a: Int, b: Int, unit : Int, op: (Int, Int) => Int) : Int = {
  if(a > b) unit
  else op(f(a), general(f)(a + 1, b, unit, op))
}

general(x => x + x)(0, 10, 0, (x, y) => x + y)