
def squareList(xs: List[Int]): List[Int] = xs match {
  case Nil => xs
  case x :: xs1 => x * x :: squareList(xs1)
}

def squareListMap(xs: List[Int]): List[Int] = {
  xs.map(x => x * x)
}


val nums = List(2, 4, 8)
squareList(nums)
squareListMap(nums)

pack(List("a", "a", "a", "b", "c", "c", "a"))
enconde(Nil)

def pack[T](xs: List[T]): List[List[T]] = xs match {
  case Nil => Nil
  case x :: xs1 =>
    val (fst, snd) = xs.span(t => t == x)
    fst :: pack(snd)
}

def enconde[T](xs: List[T]): List[(T, Int)] = pack(xs).map(l => (l.head, l.length))