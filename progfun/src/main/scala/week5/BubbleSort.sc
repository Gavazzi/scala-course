def bsort(xs: List[Int]): List[Int] = {
  val n = xs.length / 2
  if (n == 0) xs
  else {
    def merge(xs: List[Int], ys: List[Int]): List[Int] = {
      xs match {
        case Nil => ys
        case x :: xs1 =>
          ys match {
            case Nil => xs
            case y :: ys1 =>
              if (x < y) x :: merge(xs1, ys)
              else y :: merge(ys1, xs)
          }
      }
    }

    val (fns, snd) = xs splitAt n
    merge(bsort(fns), bsort(snd))
  }
}

def bsort2(xs: List[Int]): List[Int] = {
  val n = xs.length / 2
  if (n == 0) xs
  else {
    def merge(xs: List[Int], ys: List[Int]): List[Int] = (xs, ys) match {
      case (Nil, ys1) => ys
      case (xs1, Nil) => xs
      case (x :: xs1, y :: ys1) =>
        if (x < y) x :: merge(xs1, ys)
        else y :: merge(ys1, xs)

    }

    val (fns, snd) = xs splitAt n
    merge(bsort2(fns), bsort2(snd))
  }
}

def bsort3[T](xs: List[T])(implicit ord : Ordering[T]): List[T] = {
  val n = xs.length / 2
  if (n == 0) xs
  else {
    def merge(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
      case (Nil, ys1) => ys
      case (xs1, Nil) => xs
      case (x :: xs1, y :: ys1) =>
        if (ord.lt(x, y)) x :: merge(xs1, ys)
        else y :: merge(ys1, xs)

    }

    val (fns, snd) = xs splitAt n
    merge(bsort3(fns), bsort3(snd))
  }
}


val listOfInts = List(2, 4, -10, 4, 5, 6)
val fruits = List("apple, pineapple", "orange", "banana")
bsort3(listOfInts)
bsort3(fruits)

