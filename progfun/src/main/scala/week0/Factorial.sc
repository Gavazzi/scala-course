import scala.annotation.tailrec

//def factorial(n : Int) : Int = {
//  if(n == 0) 1 else n * factorial(n - 1)
//}

def factorial(n : Int) : Int = {
  def loop(acc : Int, n: Int) : Int =
    if(n == 0) acc else loop(n * acc, n - 1)

  loop(1, n)
}

factorial(4)