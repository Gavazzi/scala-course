
//def countChange(money: Int, coins: List[Int]): Int = {
//
//  def loop(acc : Int, coinsLeft : List[Int]) : Int = {
//    if(coinsLeft.isEmpty) acc
//    else loop(acc + getTotalCombinations(0, 0, coinsLeft.head, coins), coinsLeft.tail)
//  }
//
//  def getTotalCombinations(acc : Int, sum : Int, denominator : Int, coins: List[Int]) : Int = {
//    if(coins.isEmpty) acc
//    else if(sum + denominator == money)
//      getTotalCombinations(acc + 1, 0, denominator, coins.tail)
//    else if(sum + denominator < money)
//      getTotalCombinations(acc, sum + denominator, denominator, coins)
//    else getTotalCombinations(acc, 0, denominator, coins.tail)
//  }
//
//  loop(0, coins)
//}

/**
 * Exercise 3
 */
def countChange(money: Int, coins: List[Int]): Int = {

  def loop(acc : Int, coinsLeft : List[Int]) : Int = {
    if(coinsLeft.isEmpty) acc
    else loop(acc + getTotalCombinations(0, coinsLeft.head, coinsLeft.head, coins), coinsLeft.tail)
  }

  def getTotalCombinations(acc : Int, sum : Int, denominator : Int, coins: List[Int]) : Int = {
    if(coins.isEmpty) acc
    else if(sum + coins.head == money)
      getTotalCombinations(acc + 1, denominator, denominator, coins.tail)
    else if(sum + coins.head < money)
      getTotalCombinations(acc, sum + coins.head, denominator, coins)
    else getTotalCombinations(acc, 0, denominator, coins.tail)
  }

  loop(0, coins)
}


//countChange(300, List(5,10,20,50,100,200,500))
//countChange(4, List(1, 2))
countChange(4, List(1, 2, 3, 4))