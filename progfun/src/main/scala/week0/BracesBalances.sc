
def balance(chars: List[Char]): Boolean = {
  def lookup(count: Int, chars: List[Char]): Int = {
    if(chars.isEmpty) count
    else if (chars.head == '(' && count >= 0) lookup(count + 1, chars.tail)
    else if (chars.head == ')')
      if(count > 0) lookup(count - 1, chars.tail)
      else lookup(-1, chars.tail)
    else lookup(count, chars.tail)
  }

  lookup(0, chars) == 0
}

//def balance(chars: List[Char]): Boolean = {
//  def lookup(bal: Int, chars: List[Char]): Int = {
//    if(chars.isEmpty || bal < 0) -1
//    else if (chars.head == '(' && bal >= 0) lookup(bal + 1, chars.tail)
//    else if (chars.head == ')') lookup(bal - 1, chars.tail)
//    else lookup(bal, chars.tail)
//  }
//
//  lookup(0, chars) == 0
//}

balance("(if (zero? x) max (/ 1 x))".toList)
balance("I told him (that it’s not (yet) done). (But he wasn’t listening)".toList)

balance(":-)".toList)
balance("())(".toList)

balance("none".toList)