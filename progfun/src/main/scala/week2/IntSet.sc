abstract class IntSet {
  def contains(x : Int) : Boolean
  def incl(x : Int) : IntSet
  def union(that : IntSet) : IntSet
}

object Empty extends IntSet {

  override def contains(x: Int) = false

  override def incl(x: Int) = new NonEmpty(x, Empty, Empty)

  override def union(that: IntSet) = that
}

class NonEmpty(elem : Int, left : IntSet, right: IntSet) extends IntSet {

  override def contains(x: Int): Boolean = {
    if(x < elem) left.contains(x)
    else if(x > elem) right.contains(x)
    else true
  }

  override def incl(x: Int): IntSet = {
    if(x < elem) new NonEmpty(elem, left.incl(x), right)
    else if(x > elem) new NonEmpty(elem, left, right.incl(x))
    else this
  }

  override def union(that: IntSet): IntSet = {
    ((left union right) union that) incl elem
  }
}

