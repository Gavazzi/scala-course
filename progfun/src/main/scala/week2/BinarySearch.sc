val builder = List.newBuilder.+=(0)
for(i <- 0 to 1000000) {
  builder.+=(i)
}

var steps = 0
def search(x : Int, list : List[Int]) : Int = {
  steps += 1
  val half = list.size / 2
  if(list(half) == x) list(half)
  else if(list.size <= 1) -1
  else if(x > list(half)) search(x, list.takeRight(half))
  else search(x, list.slice(0, half))
}

search(213459, builder.result())
steps