package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = if (c == 0 || c == r) 1 else pascal(c - 1, r - 1) + pascal(c, r - 1)

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def lookup(count: Int, chars: List[Char]): Int = {
      if(chars.isEmpty) count
      else if (chars.head == '(' && count >= 0) lookup(count + 1, chars.tail)
      else if (chars.head == ')')
        if(count > 0) lookup(count - 1, chars.tail)
        else lookup(-1, chars.tail)
      else lookup(count, chars.tail)
    }

    lookup(0, chars) == 0
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {

    def loop(acc : Int, coinsLeft : List[Int]) : Int = {
      if(coinsLeft.isEmpty) acc
      else loop(acc + getTotalCombinations(0, coinsLeft.head, coinsLeft.head, coins), coinsLeft.tail)
    }

    def getTotalCombinations(acc : Int, sum : Int, denominator : Int, coins: List[Int]) : Int = {
      if(coins.isEmpty) acc
      else if(sum + coins.head == money)
        getTotalCombinations(acc + 1, denominator, denominator, coins.tail)
      else if(sum + coins.head < money)
        getTotalCombinations(acc, sum + coins.head, denominator, coins)
      else getTotalCombinations(acc, 0, denominator, coins.tail)
    }

    loop(0, coins)
  }
}
